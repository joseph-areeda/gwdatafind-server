# -*- coding: utf-8 -*-
# Copyright (2019) Duncan Macleod
# Licensed under GPLv3+ - see LICENSE

"""The GWDataFind server app
"""

import time

from configobj import ConfigObj
import logging
import sys

from flask import Flask

from .cache import CacheManager
from .config import get_config_path

from . import views  # pylint: disable=unused-import

__version__ = '0.1.0'


class DataFlask(Flask):
    def __init__(self, import_name, configpath, *args, **kwargs):
        super().__init__(import_name, *args, **kwargs)
        self.logger.setLevel(logging.INFO)
        lfh = logging.FileHandler('/tmp/gwdatafind.log', mode='w')
        self.logger.addHandler(lfh)

        self.config.update(ConfigObj(configpath))

        # create thread to read cache file and start
        man = self._init_manager(self.config)
        man.setDaemon(True)
        man.start()

    def _init_manager(self, conf):
        section = conf['LDRDataFindServer']
        cachefile = section['framecachefile']
        patterns = {
            key.rsplit('_', 1)[0]: section[key] for
            key in section.keys() if key.endswith('_pattern')
        }
        sleeptime = float(section.get('framecachetimeout', 60))
        self.manager = CacheManager(self, cachefile, sleeptime=sleeptime,
                                    **patterns)
        return self.manager

    def get_cache_data(self, *keys):
        self.logger.info('get_cache_data - keys: {0}'.format(keys))
        while not self.manager.ready:
            # cache file is not ready
            self.logger.debug("Waiting for cache...")
            time.sleep(.5)
        self.manager.lock.acquire()
        try:
            return self._get_cache_data(keys)
        finally:
            self.manager.lock.release()

    def _get_cache_data(self, keys):
        keys = list(keys)
        last = keys.pop(-1)
        if not keys:
            return self.manager.cache.get(last, {})
        ent = self.manager.cache.get(keys.pop(0), {})
        for key in keys:
            ent = ent[key]
        return ent.get(last, {})

    def shutdown(self):
        self.manager.lock.acquire()
        self.manager.shutdown = True
        self.manager.lock.release()
        self.manager.join()


def create_app():
    """Create an instance of the application
    """
    app = DataFlask(__name__, get_config_path())
    config_path = get_config_path()
    app = DataFlask(__name__, config_path)
    app.logger.setLevel(logging.INFO)
    app.register_blueprint(views.blueprint)
    app.logger.info('Config path: {0} name: {1}'.format(config_path, __name__))
    return app
