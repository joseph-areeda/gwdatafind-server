# -*- coding: utf-8 -*-
# Copyright (2019) Duncan Macleod
# Licensed under GPLv3+ - see LICENSE

"""Test configuration for gwdatafind_server
"""

import os
from pathlib import Path
from unittest import mock

import pytest

from gwdatafind_server import create_app


@pytest.fixture
def app():
    with mock.patch.dict(os.environ,
                         {"LDR_LOCATION": str(Path(__file__).parent)}):
        yield create_app()


@pytest.fixture
def client(app):
    return app.test_client()
