===================
 GWDataFind Server
===================

This module defines a Flask App that serves URLs based on the contents of a diskcache.

Requirements
============
* Python >= 3.4
* Flask >= 1.0.0

Apache configuration
====================

We recommend setting up a user to hold a virtual environment to support **ALL** wsgi
programs.

The service relies on the python 3 wsgi module. Confirm that it is installed ::

    $ yum info python36u-mod_wsgi
    Loaded plugins: auto-update-debuginfo, langpacks
    Installed Packages
    Name        : python36u-mod_wsgi
    Arch        : x86_64
    Version     : 4.6.2
    Release     : 1.ius.centos7
    Size        : 1.1 M
    Repo        : installed
    From repo   : lscsoft-ius
    Summary     : A WSGI interface for Python web applications in Apache
    URL         : https://modwsgi.readthedocs.io/
    License     : ASL 2.0
    Description : The mod_wsgi adapter is an Apache module that provides a WSGI compliant
                : interface for hosting Python based web applications within Apache. The
                : adapter is written completely in C code against the Apache C runtime and
                : for hosting WSGI applications within Apache has a lower overhead than using
                : existing WSGI adapters for mod_python or CGI.



The wsgi file should be installed in a location accessible to Apache but not addressable
by a browser, such as `/var/www` on SL7.

Edit the appropriate virtual host configuration to add ::

    WSGIDaemonProcess gwdatafind user=datafind group=datafind threads=5 python-home=/home/datafind/py-envs/gwpy
    WSGIScriptAlias /gwdatafind /var/www/gwdatafind_server/gwdatafind_server.wsgi

    <Directory /var/www/gwdatafind_server/gwdatafind_server.wsgi>
        WSGIProcessGroup gwdatafind
        WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
    </Directory>

Sample configuration file can be put into /etc/gwdatafind-server.ini ::

    [LDRDataFindServer]
    framecachetimeout = 10
    framecachefile = <path_to>/ascii_cache.dat
    # optional parameters
    site_exclude_pattern = ^X$
    frametype_exclude_pattern = .+EXCLUDE.+
    frametype_include_pattern = .+_TEST_\d+
    filter_preference = """{'^file': ['preferred', 'hdfs']}"""

Client API
==========

The GWDataFind Server runs as a daemon providing a RESTful interface via Apache.
There is also a client application ``gw_data_find`` for command line usage.

The URLs supported areliwted below.  They all support HTTP GET,HEAD, and OPTIONS methods.

+-----------------------------------------------------------------------+--------------------------+
| URL (add ``HTTP[S]://<host>[:<port>]/LDR`` to beginning               |  Function                |
+=======================================================================+==========================+
| ``/services/data/v1/``                                                | Show URLs (debugging)    |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>.json``                                      | Show observatories       |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>.json``                               | Show tags (frame types)  |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/latest.json``                  | Show latest              |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/<start>,<end>.json``           | Show URLs                |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/<start>,<end>/<urltype>.json`` | Show URLs of one type    |
|                                                                       | (file,URL)               |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/segments.json``                | Show all available       |
|                                                                       | segments                 |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/segments/<start>,<end>.json``  | Show avalable segments   |
|                                                                       | in a time interval       |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/latest.json``                  | Show latest URL          |
+-----------------------------------------------------------------------+--------------------------+
| ``/services/data/v1/<ext>/<site>/<tag>/<filename>.json``              | Show a single URL        |
+-----------------------------------------------------------------------+--------------------------+


